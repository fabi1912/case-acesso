package br.itau.treinamento.acesso.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente",configuration = ClienteClientConfiguration.class)
public interface ClienteClient {
	
	@GetMapping("/cliente/{id}")
	public Cliente consultaCliente (@PathVariable Long id);

}
