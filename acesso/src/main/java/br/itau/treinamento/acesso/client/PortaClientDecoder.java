package br.itau.treinamento.acesso.client;


import br.itau.treinamento.acesso.exception.PortaNotFoundExeption;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder{

	@Override
	public Exception decode(String methodKey, Response response) {
		 
		if(response.status() ==404) {	
			return new PortaNotFoundExeption();
		}
		return null;
	}
	
	

}
