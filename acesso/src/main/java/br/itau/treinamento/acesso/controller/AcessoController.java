package br.itau.treinamento.acesso.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.acesso.model.dto.AcessoRequest;
import br.itau.treinamento.acesso.model.dto.AcessoResponse;
import br.itau.treinamento.acesso.model.mapper.AcessoMapper;
import br.itau.treinamento.acesso.service.AcessoService;

@RestController
@RequestMapping("/acesso")
public class AcessoController {
	
	@Autowired
	private AcessoService acessoService;
	
    Logger logger = LoggerFactory.getLogger(AcessoController.class);


    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AcessoResponse salvaAcesso(@Valid @RequestBody AcessoRequest acessoRequest) {
    	
    	logger.debug("criando acesso " + acessoRequest.getClienteId()  + " "
    	+ acessoRequest.getPortaId());
    	
    	return AcessoMapper.acessoResponseTo(
    			acessoService.salvaAcesso(AcessoMapper.acessoTo(acessoRequest)));
 
    }
    
    
   @GetMapping("/{clienteId}/{portaId}")
    public AcessoResponse buscaAcesso(@Valid @PathVariable Long clienteId , @PathVariable Long portaId) {
   
    	logger.debug("busca acesso " + clienteId   + " " +  portaId);
    	
    	return AcessoMapper.acessoResponseTo(
    			acessoService.buscaAcessoByClientePorta(clienteId, portaId));

    }
    
   @DeleteMapping("/{clienteId}/{portaId}")
   @ResponseStatus(code = HttpStatus.NO_CONTENT)
   public void removeAcesso(@Valid @PathVariable Long clienteId , @PathVariable Long portaId) {
   	logger.debug("apagando acesso " + clienteId   + " " +  portaId);

	   acessoService.apagaAcesso(clienteId, portaId);
   }
   
   
}
