package br.itau.treinamento.acesso.client;

public class PortaClientFallback implements PortaClient {

	@Override
	public Porta consultaPorta(Long id) {
		throw new PortaUnavailableException();
	}

}
