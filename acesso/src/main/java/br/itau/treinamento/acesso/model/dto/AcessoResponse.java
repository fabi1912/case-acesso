package br.itau.treinamento.acesso.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AcessoResponse {
	
	@JsonProperty(value = "porta_id")
	private Long portaId;
	
	@JsonProperty(value = "cliente_id")
	private Long clienteId;

	public Long getPortaId() {
		return portaId;
	}

	public void setPortaId(Long portaId) {
		this.portaId = portaId;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

}
