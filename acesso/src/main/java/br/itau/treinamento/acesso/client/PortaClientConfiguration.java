package br.itau.treinamento.acesso.client;

import org.springframework.context.annotation.Bean;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;

public class PortaClientConfiguration {
	
	@Bean
	public ErrorDecoder getPortaClienteDecoder() {
		return new PortaClientDecoder();
	}

	
	@Bean
	public Feign.Builder builder(){
		FeignDecorators decorator = FeignDecorators.builder()
				.withFallback(new PortaClientFallback(),RetryableException.class)
				.build();
	
		return Resilience4jFeign.builder(decorator);
	}
}
