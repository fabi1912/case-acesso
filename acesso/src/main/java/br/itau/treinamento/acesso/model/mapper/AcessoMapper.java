package br.itau.treinamento.acesso.model.mapper;

import java.util.Date;

import br.itau.treinamento.acesso.model.Acesso;
import br.itau.treinamento.acesso.model.dto.AcessoEvento;
import br.itau.treinamento.acesso.model.dto.AcessoRequest;
import br.itau.treinamento.acesso.model.dto.AcessoResponse;
import br.itau.treinamento.acesso.producer.AcessoProducer;

public class AcessoMapper {
	
	
	public static Acesso acessoTo(AcessoRequest acessoRequest) {
		
		Acesso acesso = new Acesso();
		acesso.setClienteId(acessoRequest.getClienteId());
		acesso.setPortaId(acessoRequest.getPortaId());
		return acesso;
	}
	
	
	public static AcessoResponse acessoResponseTo(Acesso acesso) {
		AcessoResponse acessoResponse = new AcessoResponse();
		acessoResponse.setClienteId(acesso.getClienteId());
		acessoResponse.setPortaId(acesso.getPortaId());
		return acessoResponse;
	}
	

	public static AcessoEvento criarAcessoEvento(Long clienteId,Long portaId) {
		AcessoEvento acessoEvento = new AcessoEvento();
 		acessoEvento.setClienteId(clienteId);
		acessoEvento.setPortaId(portaId);
		return acessoEvento;
	}

}
