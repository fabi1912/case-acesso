package br.itau.treinamento.acesso.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(configuration = PortaClientConfiguration.class , name="porta")
public interface PortaClient {
	
	@GetMapping("/porta/{id}")
	public Porta consultaPorta(@PathVariable Long id);

}
