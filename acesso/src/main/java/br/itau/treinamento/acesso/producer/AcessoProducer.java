package br.itau.treinamento.acesso.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.itau.treinamento.acesso.model.dto.AcessoEvento;

@Service
public class AcessoProducer {
	
	
	@Autowired
    private KafkaTemplate<String, AcessoEvento> producer;
	
	public void enviarLog(AcessoEvento acessoEvento) {
      producer.send("spec4-fabiana-hisako-1", acessoEvento);
	}

}
