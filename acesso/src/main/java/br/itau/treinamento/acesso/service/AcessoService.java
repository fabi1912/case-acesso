package br.itau.treinamento.acesso.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.treinamento.acesso.client.ClienteClient;
import br.itau.treinamento.acesso.client.PortaClient;
import br.itau.treinamento.acesso.exception.AcessoNotFoundException;
import br.itau.treinamento.acesso.model.Acesso;
import br.itau.treinamento.acesso.model.dto.AcessoEvento;
import br.itau.treinamento.acesso.model.mapper.AcessoMapper;
import br.itau.treinamento.acesso.producer.AcessoProducer;
import br.itau.treinamento.acesso.repository.AcessoRepository;

@Service
public class AcessoService {
	
	@Autowired
	private AcessoRepository acessoRepository;
	
	@Autowired
	private ClienteClient clienteClient; 
	
	@Autowired
	private PortaClient portaClient;
	
	@Autowired
	private AcessoProducer acessoProducer;
	
	public Acesso salvaAcesso (Acesso acesso) {
		
		clienteClient.consultaCliente(acesso.getClienteId());
		portaClient.consultaPorta(acesso.getPortaId());
		return acessoRepository.save(acesso);
	}
	
	public void apagaAcesso(Long clienteId, Long portaId) {
		
		 acessoRepository.removeAcessoByClientePorta(portaId, clienteId);
	}
	
	public Acesso buscaAcessoByClientePorta(Long clienteId,Long portaId) {
		
		List<Acesso> acessoList = acessoRepository.buscaAcessobyClientePorta(portaId, clienteId);		
		AcessoEvento acessoEvento = AcessoMapper.criarAcessoEvento(clienteId, portaId);
		
		if(acessoList!=null && !acessoList.isEmpty()) {
			
			acessoEvento.setAcesso(true);			
			acessoProducer.enviarLog(acessoEvento);			
			return acessoList.get(0);
		}
		
		acessoEvento.setAcesso(false);			
		acessoProducer.enviarLog(acessoEvento);			
		throw new AcessoNotFoundException();
		
	}
	

}
