package br.itau.treinamento.acesso.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Servico de porta indisponivel")
public class PortaUnavailableException extends RuntimeException {

}
