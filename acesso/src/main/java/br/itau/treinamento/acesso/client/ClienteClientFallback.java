package br.itau.treinamento.acesso.client;


public class ClienteClientFallback implements ClienteClient {

	@Override
	public Cliente consultaCliente(Long id) {
		throw new ClienteUnavailableException();
	}

}
