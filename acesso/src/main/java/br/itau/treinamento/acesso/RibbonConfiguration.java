package br.itau.treinamento.acesso;

import org.springframework.context.annotation.Bean;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;

public class RibbonConfiguration {
	
	@Bean
	public IRule getRule() {
		 return new RandomRule();
	}

}
