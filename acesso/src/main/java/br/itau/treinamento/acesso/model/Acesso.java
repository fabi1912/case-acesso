package br.itau.treinamento.acesso.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Acesso {
	
	@Id
	@Column(name="acesso_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="acesso_cliente_id")
	@NotNull
	private Long clienteId;
	
	@Column(name="acesso_porta_id")
	@NotNull
	private Long portaId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

	public Long getPortaId() {
		return portaId;
	}

	public void setPortaId(Long portaId) {
		this.portaId = portaId;
	}
	
	
	

}
