package br.itau.treinamento.acesso.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.itau.treinamento.acesso.model.Acesso;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {
	
	
	String QUERY_DELETE_ACESSO_CLIENTE_PORTA ="DELETE from acesso"
			+ " WHERE acesso_porta_id = :portaId AND acesso_cliente_id = :clienteId";

	String QUERY_BUSCA_ACESSO_CLIENTE_PORTA ="SELECT * from acesso"
			+ " WHERE acesso_porta_id = :portaId AND acesso_cliente_id = :clienteId";

	
	
	@Modifying
	@Transactional
	@Query(value = QUERY_DELETE_ACESSO_CLIENTE_PORTA,nativeQuery = true)
	public void removeAcessoByClientePorta(Long portaId, Long clienteId);
	
	@Modifying
	@Transactional
	@Query(value = QUERY_BUSCA_ACESSO_CLIENTE_PORTA, nativeQuery= true)
	public List<Acesso> buscaAcessobyClientePorta(Long portaId,Long clienteId);
	
	
}
