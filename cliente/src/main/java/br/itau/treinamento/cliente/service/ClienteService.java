package br.itau.treinamento.cliente.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.treinamento.cliente.exception.ClienteNotFoundException;
import br.itau.treinamento.cliente.model.Cliente;
import br.itau.treinamento.cliente.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	public Cliente salvaCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	
	public Cliente buscaCliente (Long clienteId) {
		
		Optional<Cliente> cliente =clienteRepository.findById(clienteId);
		
		if(cliente.isPresent()) {
			return cliente.get();
		}
		
		throw new ClienteNotFoundException();
	}

}
