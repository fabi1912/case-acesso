package br.itau.treinamento.cliente.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.cliente.model.dto.ClienteRequest;
import br.itau.treinamento.cliente.model.dto.ClienteResponse;
import br.itau.treinamento.cliente.model.mapper.ClienteMapper;
import br.itau.treinamento.cliente.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService ClienteService;
	
    Logger logger = LoggerFactory.getLogger(ClienteController.class);
	
	@PostMapping
	@ResponseStatus(code=HttpStatus.CREATED)
	public ClienteResponse salvaCliente(@Valid @RequestBody ClienteRequest clienteRequest) {
		
		logger.debug("Salva cliente " + clienteRequest.getNome());
		
		return ClienteMapper.toClienteResponse(
				ClienteService.salvaCliente(ClienteMapper.toCliente(clienteRequest)));
	}
	
	@GetMapping("/{id}")
	public ClienteResponse consultaCliente(@Valid @PathVariable Long id) {
		
		logger.debug("Buscando cliente " + id);
		
		return ClienteMapper.toClienteResponse(ClienteService.buscaCliente(id));
	}
}
