package br.itau.treinamento.cliente.model.mapper;

import br.itau.treinamento.cliente.model.Cliente;
import br.itau.treinamento.cliente.model.dto.ClienteRequest;
import br.itau.treinamento.cliente.model.dto.ClienteResponse;

public class ClienteMapper {
	

	public static Cliente toCliente(ClienteRequest clienteRequest) {
		Cliente cliente = new Cliente();
		cliente.setNome(clienteRequest.getNome());
		return cliente;
	}
	
	public static ClienteResponse toClienteResponse(Cliente cliente) {
		
		ClienteResponse clienteResponse = new ClienteResponse();
		clienteResponse.setId(cliente.getId());
		clienteResponse.setNome(cliente.getNome());
		return clienteResponse;
		
	}
	
}
