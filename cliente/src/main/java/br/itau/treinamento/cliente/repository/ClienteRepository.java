package br.itau.treinamento.cliente.repository;

import org.springframework.data.repository.CrudRepository;

import br.itau.treinamento.cliente.model.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{

}
