package br.itau.treinamento.porta.repository;

import org.springframework.data.repository.CrudRepository;

import br.itau.treinamento.porta.model.Porta;

public interface PortaRepository extends CrudRepository<Porta,Long>{

}
