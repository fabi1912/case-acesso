package br.itau.treinamento.porta.model.mapper;

import br.itau.treinamento.porta.model.Porta;
import br.itau.treinamento.porta.model.dto.PortaRequest;
import br.itau.treinamento.porta.model.dto.PortaResponse;

public class PortaMapper {
	
	
	public static Porta toPorta(PortaRequest portaRequest) {
		
		Porta porta = new Porta();
		porta.setAndar(portaRequest.getAndar());
		porta.setSala(portaRequest.getSala());
		return porta;
	}
	
	public static PortaResponse toPortaResponse(Porta porta) {
		
		PortaResponse portaResponse = new PortaResponse();
		portaResponse.setAndar(porta.getAndar());
		portaResponse.setSala(porta.getSala());
		portaResponse.setId(porta.getId());
		
		return portaResponse;
	}
	

}
