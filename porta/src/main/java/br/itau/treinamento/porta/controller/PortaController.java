package br.itau.treinamento.porta.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.porta.model.dto.PortaRequest;
import br.itau.treinamento.porta.model.dto.PortaResponse;
import br.itau.treinamento.porta.model.mapper.PortaMapper;
import br.itau.treinamento.porta.service.PortaService;

@RestController
@RequestMapping("/porta")
public class PortaController {
	
	@Autowired
	private PortaService portaService;
	
    Logger logger = LoggerFactory.getLogger(PortaController.class);

	
	@PostMapping
	@ResponseStatus(code=HttpStatus.CREATED)
	public PortaResponse salvaPorta(@Valid @RequestBody PortaRequest portaRequest) {
		
		logger.debug("salvando porta " + portaRequest.getSala());
		
		return PortaMapper.toPortaResponse(
				portaService.salvaPorta(PortaMapper.toPorta(portaRequest)));
		
	}
	
	@GetMapping("/{id}")
	public PortaResponse buscaPorta(@Valid @PathVariable Long id) {
		
		logger.debug("buscando porta " + id);
		return PortaMapper.toPortaResponse(portaService.buscaPorta(id));

	}

}
