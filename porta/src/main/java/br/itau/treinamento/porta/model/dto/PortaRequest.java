package br.itau.treinamento.porta.model.dto;

import javax.validation.constraints.NotBlank;

public class PortaRequest {
	
	
	@NotBlank
	private String andar;
	
	@NotBlank
	private String sala;

	public String getAndar() {
		return andar;
	}

	public void setAndar(String andar) {
		this.andar = andar;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}
	
	

}
