package br.itau.treinamento.porta.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.treinamento.porta.exception.PortaNotFoundExeption;
import br.itau.treinamento.porta.model.Porta;
import br.itau.treinamento.porta.repository.PortaRepository;

@Service
public class PortaService {
	
	@Autowired
	PortaRepository portaRepository;
	
	public Porta salvaPorta(Porta porta) {
		return portaRepository.save(porta);
	}
	
	public Porta buscaPorta(Long id) {
		
		Optional<Porta> porta = portaRepository.findById(id);
		
		if(porta.isPresent()) {
			return porta.get();
		}
		
		throw new PortaNotFoundExeption();
	}

}
